- [Requirements](#org1bceba4)
- [Prepare the environment](#org4002fb1)
- [How to run the tests](#org4801ec1)
- [Report](#org347b329)
  - [Generate report manually](#org0cf7f3b)
- [Running CI/CD](#org347b330)



<a id="org1bceba4"></a>

# Requirements

-   [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) >= 1.8
-   [Maven](https://maven.apache.org/)


<a id="org4002fb1"></a>

# Prepare the environment

1. Clone this repository
2. Install JDK
3. Install maven
4. Next commands should work correct:
    1. Check java version:
        
            java -version
            
            java version "1.8.0_202"
            Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
            Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, mixed mode)
    2. Check maven version:
        
            mvn -version
            
            Apache Maven 3.6.0 (NON-CANONICAL_2018-11-06T03:14:22+01:00_root; 2018-11-06T04:14:22+02:00)
            Maven home: /opt/maven
            Java version: 1.8.0_202, vendor: Oracle Corporation, runtime: /usr/lib/jvm/java-8-jdk/jre
            Default locale: en_US, platform encoding: UTF-8
            OS name: "linux", version: "5.0.7-arch1-1-arch", arch: "amd64", family: "unix"
5. Check file `src/test/resources/prod.properties`. You can edit properties in this file or create new properties file with required properties.


<a id="org4801ec1"></a>

# How to run the tests

Use the next command:

```shell
mvn clean test allure:serve -Denv=prod
```

It will run all tests and build report. You can set name of pre-defined properties in `src/test/resources` directory (without `.properties` suffix) as `-Denv` argument to select test environment. In this example only `prod.properties` is defined.


<a id="org347b329"></a>

# Report

Report will be generated automatically after all tests. A browser will open automatically.

<a id="org0cf7f3b"></a>

## Generate report manually

* Use the next command:

```shell
mvn allure:report
```

Report can be found here: `target/site/allure-maven-plugin/index.html`. 

* Or you can run this command:

```shell
mvn allure:serve
```

It will generate a report and run a web server with generated report.

<a id="org347b330"></a>

# Running CI/CD

The project has a CI/CD pipeline set up in Gitlab. It will run on any commit automatically. It is possible to execute it manually:
* Go to **CI/CD -> Pipelines**
* Click **Run pipeline** button

Report can be found in artifacts after pipeline finishes. It can be downloaded or viewed using Gitlab:
* Open latest finished pipeline
* Open **test-job**
* Click **Browse** button in job artifacts section to the right
* Navigate to **target -> site -> allure-maven-plugin**
* Open **index.html**
