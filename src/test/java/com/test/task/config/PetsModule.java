package com.test.task.config;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.test.task.model.Category;
import com.test.task.model.Pet;
import com.test.task.model.Tag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PetsModule extends AbstractModule {

    @Provides
    Pet getTestPet() {
        Pet pet = new Pet();
        pet.setId(999L);
        pet.setName("Test name");

        Category category = new Category();
        category.setId(1L);
        category.setName("Test category");
        pet.setCategory(category);

        List<String> photoUrls = new ArrayList<>();
        photoUrls.add("photo1");
        photoUrls.add("photo2");
        pet.setPhotoUrls(photoUrls);

        List<Tag> tags = new ArrayList<>();
        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("tag1");
        tags.add(tag1);

        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("tag2");
        tags.add(tag2);
        pet.setTags(tags);

        pet.setStatus("pending");

        return pet;
    }

    @Provides
    @Named("Invalid pets")
    Map<String, Object> getInvalidPets() {
        Map<String, Object> invalids = new HashMap<>();

        // Invalid string
        invalids.put("Invalid string as body", "invalid");

        // Invalid request
        Map<String, String> invalidRequest = new HashMap<>();
        invalidRequest.put("missingField", "Some value");
        invalids.put("Invalid request body", invalidRequest);

        return invalids;
    }
}
