package com.test.task.config;

import com.google.inject.Provider;
import org.aeonbits.owner.ConfigFactory;

public class MainConfigProvider implements Provider<MainConfig> {

    @Override
    public MainConfig get() {
        return ConfigFactory.create(MainConfig.class);
    }
}
