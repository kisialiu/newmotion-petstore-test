package com.test.task.config;

import org.aeonbits.owner.Config;

@Config.Sources("classpath:${env}.properties")
public interface MainConfig extends Config {

    @Key("at.base_uri")
    @DefaultValue("https://petstore.swagger.io")
    String getBaseURI();

    @Key("at.base_path")
    @DefaultValue("/v2")
    String getBasePath();
}
