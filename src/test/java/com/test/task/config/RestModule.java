package com.test.task.config;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class RestModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(MainConfig.class).toProvider(MainConfigProvider.class).in(Singleton.class);
    }

    @Provides
    @Named("JSON spec")
    RequestSpecification requestSpecJSON(MainConfig config) {
        return new RequestSpecBuilder()
                .setBaseUri(config.getBaseURI())
                .setBasePath(config.getBasePath())
                .addFilter(new AllureRestAssured())
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .build();
    }

    @Provides
    @Named("URLENC spec")
    RequestSpecification requestSpecUrlEnc(MainConfig config) {
        return new RequestSpecBuilder()
                .setBaseUri(config.getBaseURI())
                .setBasePath(config.getBasePath())
                .addFilter(new AllureRestAssured())
                .setContentType(ContentType.URLENC)
                .setAccept(ContentType.JSON)
                .build();
    }

    @Provides
    @Named("Multipart spec")
    RequestSpecification requestSpecMultipart(MainConfig config) {
        return new RequestSpecBuilder()
                .setBaseUri(config.getBaseURI())
                .setBasePath(config.getBasePath())
                .addFilter(new AllureRestAssured())
                .setAccept(ContentType.JSON)
                .build();
    }
}
