package com.test.task.constant;

public final class PetsEndpoint {

    public static final String PET = "/pet";
    public static final String PET_BY_ID = PET + "/{petId}";
    public static final String PET_BY_STATUS = PET + "/findByStatus";
    public static final String PET_UPLOAD = PET_BY_ID + "/uploadImage";

    private PetsEndpoint() {
    }
}
