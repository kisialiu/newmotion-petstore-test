package com.test.task.model;

import lombok.Data;

@Data
public class Category {
    private Long id;
    private String name;
}
