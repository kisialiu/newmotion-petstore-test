package com.test.task.tests;

import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public interface TestLifecycleLogger {

    Logger LOGGER = LoggerFactory.getLogger(TestLifecycleLogger.class);

    @BeforeAll
    default void beforeAllTests() {
        LOGGER.info("Start test suite");
    }

    @AfterAll
    default void afterAllTests() {
        LOGGER.info("Test suite finished");
    }

    @BeforeEach
    default void beforeEachTest(TestInfo testInfo) {
        LOGGER.info("TEST STARTED: {}", testInfo.getDisplayName());
    }

    @AfterEach
    default void afterEachTest(TestInfo testInfo) {
        LOGGER.info("TEST FINISHED: {}", testInfo.getDisplayName());
    }
}
