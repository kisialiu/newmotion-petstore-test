package com.test.task.tests.pet;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.test.task.config.PetsModule;
import com.test.task.config.RestModule;
import com.test.task.constant.PetsEndpoint;
import com.test.task.model.Pet;
import com.test.task.tests.TestLifecycleLogger;
import io.qameta.allure.*;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import name.falgout.jeffrey.testing.junit.guice.GuiceExtension;
import name.falgout.jeffrey.testing.junit.guice.IncludeModule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.restassured.RestAssured.given;

@Feature("Pets")
@Story("Delete pet")
@Slf4j
@ExtendWith(GuiceExtension.class)
@IncludeModule(RestModule.class)
@IncludeModule(PetsModule.class)
public class DeletePetTestSuite implements TestLifecycleLogger {

    @Inject
    @Named("JSON spec")
    private RequestSpecification requestSpec;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Successful")
    @DisplayName("Successful")
    void testDeletePetSuccessfully(Pet testPet) {
        log.info("Create new pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .body(testPet)
                .post(PetsEndpoint.PET);

        log.info("Delete pet with ID {}", testPet.getId());
        given(requestSpec)
                .delete(PetsEndpoint.PET_BY_ID, testPet.getId())
                .then()
                .statusCode(200);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Not found")
    @DisplayName("Not found")
    void testDeletePetNotFound(Pet testPet) {
        log.info("Delete pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .delete(PetsEndpoint.PET_BY_ID, testPet.getId());

        log.info("Try to delete the same pet again");
        given(requestSpec)
                .delete(PetsEndpoint.PET_BY_ID, testPet.getId())
                .then()
                .statusCode(404);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Invalid petID")
    @DisplayName("Invalid petID")
    void testDeletePetInvalidID() {
        log.info("Try to delete pet by invalid petID");
        given(requestSpec)
                .delete(PetsEndpoint.PET_BY_ID, "invalid")
                .then()
                .statusCode(400);
    }
}
