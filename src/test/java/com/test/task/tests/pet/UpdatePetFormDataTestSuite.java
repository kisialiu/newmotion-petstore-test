package com.test.task.tests.pet;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.test.task.config.PetsModule;
import com.test.task.config.RestModule;
import com.test.task.constant.PetsEndpoint;
import com.test.task.model.Pet;
import com.test.task.tests.TestLifecycleLogger;
import io.qameta.allure.*;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import name.falgout.jeffrey.testing.junit.guice.GuiceExtension;
import name.falgout.jeffrey.testing.junit.guice.IncludeModule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.restassured.RestAssured.given;

@Feature("Pets")
@Story("Update a pet in store with form data")
@Slf4j
@ExtendWith(GuiceExtension.class)
@IncludeModule(RestModule.class)
@IncludeModule(PetsModule.class)
public class UpdatePetFormDataTestSuite implements TestLifecycleLogger {

    @Inject
    @Named("JSON spec")
    private RequestSpecification requestSpec;

    @Inject
    @Named("URLENC spec")
    private RequestSpecification requestSpecUrlEnc;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Successful")
    @DisplayName("Successful")
    void testUpdateDataSuccessful(Pet testPet) {
        log.info("Create new pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .body(testPet)
                .post(PetsEndpoint.PET);

        testPet.setName("Updated name");
        testPet.setStatus("sold");

        log.info("Update pet with ID {} using form data", testPet.getId());
        given(requestSpecUrlEnc)
                .formParam("name", testPet.getName())
                .formParam("status", testPet.getStatus())
                .post(PetsEndpoint.PET_BY_ID, testPet.getId())
                .then()
                .statusCode(200);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Not found")
    @DisplayName("Not found")
    void testUpdateDataNotFound(Pet testPet) {
        log.info("Delete pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .delete(PetsEndpoint.PET_BY_ID, testPet.getId());

        log.info("Try to update not existing pet using form data");
        given(requestSpecUrlEnc)
                .formParam("name", testPet.getName())
                .formParam("status", testPet.getStatus())
                .post(PetsEndpoint.PET_BY_ID, testPet.getId())
                .then()
                .statusCode(404);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Invalid petID")
    @DisplayName("Invalid petID")
    void testUpdateDataInvalidID(Pet testPet) {
        log.info("Create new pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .body(testPet)
                .post(PetsEndpoint.PET);

        log.info("Try to update pet by invalid petID");
        given(requestSpecUrlEnc)
                .formParam("name", testPet.getName())
                .formParam("status", testPet.getStatus())
                .post(PetsEndpoint.PET_BY_ID, "invalid")
                .then()
                .statusCode(400);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Invalid params")
    @DisplayName("Invalid params")
    void testUpdateDataMissingName(Pet testPet) {
        log.info("Create new pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .body(testPet)
                .post(PetsEndpoint.PET);

        log.info("Try to update pet using invalid params");
        given(requestSpecUrlEnc)
                .formParam("invalid", "somevalue")
                .post(PetsEndpoint.PET_BY_ID, testPet.getId())
                .then()
                .statusCode(400);
    }
}
