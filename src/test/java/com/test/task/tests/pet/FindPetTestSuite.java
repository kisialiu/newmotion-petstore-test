package com.test.task.tests.pet;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.test.task.config.PetsModule;
import com.test.task.config.RestModule;
import com.test.task.constant.PetsEndpoint;
import com.test.task.model.Pet;
import com.test.task.tests.TestLifecycleLogger;
import io.qameta.allure.*;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import name.falgout.jeffrey.testing.junit.guice.GuiceExtension;
import name.falgout.jeffrey.testing.junit.guice.IncludeModule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Feature("Pets")
@Story("Find pet by status")
@Slf4j
@ExtendWith(GuiceExtension.class)
@IncludeModule(RestModule.class)
@IncludeModule(PetsModule.class)
public class FindPetTestSuite implements TestLifecycleLogger {

    @Inject
    @Named("JSON spec")
    private RequestSpecification requestSpecification;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Successful")
    @DisplayName("Successful")
    void testFindPetByStatusSuccessfully(Pet testPet) {
        log.info(
                "Create new pet with ID {} and status {}, ignore errors",
                testPet.getId(),
                testPet.getStatus());
        given(requestSpecification)
                .body(testPet)
                .post(PetsEndpoint.PET);

        Type petListType =
                new TypeToken<ArrayList<Pet>>() {
                    private static final long serialVersionUID = 192670116248545338L;
                }.getType();

        log.info("Find all pets in status 'pending'");
        List<Pet> pets =
                given(requestSpecification)
                        .param("status", testPet.getStatus())
                        .expect()
                        .statusCode(200)
                        .body(matchesJsonSchemaInClasspath("schemas/pets/pet-list-schema.json"))
                        .when()
                        .get(PetsEndpoint.PET_BY_STATUS)
                        .as(petListType);

        assertTrue(pets.contains(testPet));
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Invalid status")
    @DisplayName("Invalid status")
    void testFindPetInvalidStatus() {
        log.info("Find pets, invalid status");
        given(requestSpecification)
                .param("status", "invalid")
                .get(PetsEndpoint.PET_BY_STATUS)
                .then()
                .statusCode(400);
    }
}
