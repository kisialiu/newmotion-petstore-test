package com.test.task.tests.pet;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.test.task.config.PetsModule;
import com.test.task.config.RestModule;
import com.test.task.constant.PetsEndpoint;
import com.test.task.model.Pet;
import com.test.task.tests.TestLifecycleLogger;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import name.falgout.jeffrey.testing.junit.guice.GuiceExtension;
import name.falgout.jeffrey.testing.junit.guice.IncludeModule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Feature("Pets")
@Story("Get pet by ID")
@Slf4j
@ExtendWith(GuiceExtension.class)
@IncludeModule(RestModule.class)
@IncludeModule(PetsModule.class)
public class GetPetsTestSuite implements TestLifecycleLogger {

    @Inject
    @Named("JSON spec")
    private RequestSpecification requestSpec;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @DisplayName("Successfully get pet by ID")
    void testGetPetsSuccessfully(Pet testPet) {
        log.info("Create new pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .body(testPet)
                .post(PetsEndpoint.PET);

        log.info("Get pet and check ID {}", testPet.getId());
        Pet pet =
                given(requestSpec)
                        .expect()
                        .statusCode(200)
                        .body(matchesJsonSchemaInClasspath("schemas/pets/pet-schema.json"))
                        .when()
                        .get(PetsEndpoint.PET_BY_ID, testPet.getId())
                        .as(Pet.class);

        assertEquals(testPet, pet);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Not found")
    void testPetNotFound(Pet testPet) {
        log.info("Delete pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .delete(PetsEndpoint.PET_BY_ID, testPet.getId());

        log.info("Try to get deleted pet by ID {}", testPet.getId());
        given(requestSpec)
                .contentType(ContentType.URLENC)
                .expect()
                .statusCode(404)
                .body(matchesJsonSchemaInClasspath("schemas/pets/pet-not-found-schema.json"))
                .when()
                .get(PetsEndpoint.PET_BY_ID, testPet.getId());
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Invalid petID")
    void testGetPetInvalidID() {
        log.info("Try to get pet by invalid petID");
        given(requestSpec)
                .get(PetsEndpoint.PET_BY_ID, "invalid")
                .then()
                .statusCode(400);
    }
}
