package com.test.task.tests.pet;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.test.task.config.PetsModule;
import com.test.task.config.RestModule;
import com.test.task.constant.PetsEndpoint;
import com.test.task.model.Pet;
import com.test.task.tests.TestLifecycleLogger;
import io.qameta.allure.*;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import name.falgout.jeffrey.testing.junit.guice.GuiceExtension;
import name.falgout.jeffrey.testing.junit.guice.IncludeModule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Feature("Pets")
@Story("Update existing pet")
@Slf4j
@ExtendWith(GuiceExtension.class)
@IncludeModule(RestModule.class)
@IncludeModule(PetsModule.class)
public class UpdatePetTestSuite implements TestLifecycleLogger {

    @Inject
    @Named("JSON spec")
    private RequestSpecification requestSpec;

    @Inject
    @Named("Invalid pets")
    private static Map<String, Object> invalidPets;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Successful")
    @DisplayName("Successful")
    void testUpdatePetSuccessfully(Pet testPet) {
        log.info("Create new pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .body(testPet)
                .post(PetsEndpoint.PET);

        // Update some fields
        testPet.setName("Updated name");
        testPet.setStatus("available");

        log.info("Update existing pet with ID {}", testPet.getId());
        Pet pet =
                given(requestSpec)
                        .body(testPet)
                        .expect()
                        .statusCode(200)
                        .body(matchesJsonSchemaInClasspath("schemas/pets/pet-schema.json"))
                        .when()
                        .put(PetsEndpoint.PET)
                        .as(Pet.class);

        assertEquals(testPet, pet);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Not found")
    @DisplayName("Not found")
    void testUpdatePetNotFound(Pet testPet) {
        log.info("Delete pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .delete(PetsEndpoint.PET_BY_ID, testPet.getId());

        log.info("Try to update deleted pet with ID {}", testPet.getId());
        given(requestSpec)
                .body(testPet)
                .put(PetsEndpoint.PET)
                .then()
                .statusCode(404);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Invalid status")
    @DisplayName("Invalid status")
    void testUpdatePetInvalidStatus(Pet testPet) {
        testPet.setStatus("invalid");
        log.info("Update pet, set invalid status");
        given(requestSpec)
                .body(testPet)
                .put(PetsEndpoint.PET)
                .then()
                .statusCode(400);
    }

    @ParameterizedTest
    @MethodSource("invalidProvider")
    @Severity(SeverityLevel.NORMAL)
    @Description("Validation checks")
    @DisplayName("Validation checks")
    void testUPdatePetValidation(String invalid) {
        log.info("Send invalid request");
        given(requestSpec)
                .body(invalidPets.get(invalid))
                .put(PetsEndpoint.PET)
                .then()
                .statusCode(400);
    }

    static Stream<String> invalidProvider() {
        return invalidPets.keySet().stream();
    }
}
