package com.test.task.tests.pet;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.test.task.config.PetsModule;
import com.test.task.config.RestModule;
import com.test.task.constant.PetsEndpoint;
import com.test.task.model.Pet;
import com.test.task.tests.TestLifecycleLogger;
import io.qameta.allure.*;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import name.falgout.jeffrey.testing.junit.guice.GuiceExtension;
import name.falgout.jeffrey.testing.junit.guice.IncludeModule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;

import static io.restassured.RestAssured.given;

@Feature("Pets")
@Story("Upload photo")
@Slf4j
@ExtendWith(GuiceExtension.class)
@IncludeModule(RestModule.class)
@IncludeModule(PetsModule.class)
public class UploadPhotoTestSuite implements TestLifecycleLogger {

    @Inject
    @Named("JSON spec")
    private RequestSpecification requestSpec;

    @Inject
    @Named("Multipart spec")
    private RequestSpecification requestSpecMultipart;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Successful")
    @DisplayName("Successful")
    void testUploadPhotoSuccessfully(Pet testPet) {
        log.info("Create new pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .body(testPet)
                .post(PetsEndpoint.PET);

        log.info("Upload photo for pet by ID {}", testPet.getId());
        given(requestSpecMultipart)
                .multiPart("file", new File("img/photo.jpg"))
                .multiPart("additionalMetadata", "Test metadata")
                .post(PetsEndpoint.PET_UPLOAD, testPet.getId())
                .then()
                .statusCode(200);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Not found")
    void testUploadPhotoNotFound(Pet testPet) {
        log.info("Delete pet with ID {}, ignore errors", testPet.getId());
        given(requestSpec)
                .delete(PetsEndpoint.PET_BY_ID, testPet.getId());

        log.info("Upload photo for pet by ID {}", testPet.getId());
        given(requestSpecMultipart)
                .multiPart("file", new File("img/photo.jpg"))
                .multiPart("additionalMetadata", "Test metadata")
                .post(PetsEndpoint.PET_UPLOAD, testPet.getId())
                .then()
                .statusCode(404);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @DisplayName("Invalid petID")
    void testUploadPhotoInvalidPetID() {
        log.info("Try to upload photo by invalid petID");
        given(requestSpecMultipart)
                .multiPart("file", new File("img/photo.jpg"))
                .multiPart("additionalMetadata", "Test metadata")
                .post(PetsEndpoint.PET_UPLOAD, "invalid")
                .then()
                .statusCode(400);
    }
}
