package com.test.task.tests.pet;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.test.task.config.PetsModule;
import com.test.task.config.RestModule;
import com.test.task.constant.PetsEndpoint;
import com.test.task.model.Pet;
import com.test.task.tests.TestLifecycleLogger;
import io.qameta.allure.*;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;
import name.falgout.jeffrey.testing.junit.guice.GuiceExtension;
import name.falgout.jeffrey.testing.junit.guice.IncludeModule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;

@Feature("Pets")
@Story("Add new pet")
@Slf4j
@ExtendWith(GuiceExtension.class)
@IncludeModule(RestModule.class)
@IncludeModule(PetsModule.class)
public class CreatePetTestSuite implements TestLifecycleLogger {

    @Inject
    @Named("Invalid pets")
    private static Map<String, Object> invalidPets;
    @Inject
    @Named("JSON spec")
    private RequestSpecification requestSpecification;

    static Stream<String> invalidProvider() {
        return invalidPets.keySet().stream();
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Successful")
    @DisplayName("Successful")
    void testAddPetSuccessfully(Pet testPet) {
        testPet.setId(123L);
        log.info("Create new pet with ID {}", testPet.getId());
        given(requestSpecification)
                .body(testPet)
                .post(PetsEndpoint.PET)
                .then()
                .statusCode(200);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Conflict")
    @DisplayName("Conflict")
    void testAddPetConflict(Pet testPet) {
        testPet.setId(2L);
        log.info("Create new pet with ID {}", testPet.getId());
        given(requestSpecification)
                .body(testPet)
                .post(PetsEndpoint.PET)
                .then()
                .statusCode(200);

        log.info("Try to create the same pet again");
        given(requestSpecification)
                .body(testPet)
                .post(PetsEndpoint.PET)
                .then()
                .statusCode(409);
    }

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Invalid status")
    @DisplayName("Invalid status")
    void testAddPetInvalid(Pet testPet) {
        testPet.setStatus("invalid");
        log.info("Create pet with invalid status");
        given(requestSpecification)
                .body(testPet)
                .post(PetsEndpoint.PET)
                .then()
                .statusCode(400);
    }

    @ParameterizedTest
    @MethodSource("invalidProvider")
    @Severity(SeverityLevel.NORMAL)
    @Description("Validation checks")
    @DisplayName("Validation checks")
    void testAddPetValidations(String invalid) {
        log.info("Send invalid request");
        given(requestSpecification)
                .body(invalidPets.get(invalid))
                .post(PetsEndpoint.PET)
                .then()
                .statusCode(400);
    }
}
